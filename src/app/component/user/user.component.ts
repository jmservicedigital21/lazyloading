import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  headers = ['ID', 'Nom', 'Prenom', 'Adresse', 'Tel', 'Email'];

  users =[
    {
      ID: 1,
      Nom: 'renault',
      Prenom: 'michel',
      Adresse: '1 rue carnot',
      Tel: '0148758470',
      Email: 'titi@toto.fr'
    },
    {
      ID: 2,
      Nom: 'peugeot',
      Prenom: 'michel',
      Adresse: '1 rue carnot',
      Tel: '0148758470',
      Email: 'titi@toto.fr'
    },
    {
      ID: 3,
      Nom: 'citroen',
      Prenom: 'michel',
      Adresse: '1 rue carnot',
      Tel: '0148758470',
      Email: 'titi@toto.fr'
    },
    {
      ID: 4,
      Nom: 'bmw',
      Prenom: 'michel',
      Adresse: '1 rue carnot',
      Tel: '0148758470',
      Email: 'titi@toto.fr'
    },
    {
      ID: 5,
      Nom: 'mercedes',
      Prenom: 'michel',
      Adresse: '1 rue carnot',
      Tel: '0148758470',
      Email: 'titi@toto.fr'
    },
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
