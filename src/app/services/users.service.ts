import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Users} from '../model/users';
import {log} from 'util';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

   public readonly baseUrl = 'api/users.json';

  constructor(private http: HttpClient) { }

  public getUsers(): Observable<Users[] >{
    return this.http.get<Users[]>(this.baseUrl).pipe(
      tap(users => console.log('users', users)),
      catchError(this.handleError)
    );
  }
  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {

      console.error('An error occurred:', error.error);
    } else {

      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }

    return throwError(
      'Something bad happened; please try again later.');
  }
}
