import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TableauComponent } from './tableau.component';
import {TableauRoutingModule} from './tableau-routing.module';



@NgModule({
  declarations: [TableauComponent],
  imports: [
    CommonModule, TableauRoutingModule
  ],
  exports: [
    TableauComponent
  ]
})
export class TableauModule{}
