export interface Users {
  ID: number;
  Nom: string;
  Prenom: string;
  Adresse: string;
  Tel: string;
  email: string;
}
