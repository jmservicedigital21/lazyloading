import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule} from '@angular/common/http'

import { AppComponent } from './app.component';
import { HeaderComponent } from './structure/header/header.component';
import { HomeComponent } from './component/home/home.component';
import { DetailtableauComponent } from './component/detailtableau/detailtableau.component';
import { DetailUserComponent } from './component/detail-user/detail-user.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    DetailtableauComponent,
    DetailUserComponent




  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
