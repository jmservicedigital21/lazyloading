import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailtableauComponent } from './detailtableau.component';

describe('DetailtableauComponent', () => {
  let component: DetailtableauComponent;
  let fixture: ComponentFixture<DetailtableauComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailtableauComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailtableauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
