import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './component/home/home.component';
import {TableauComponent} from './component/tableau/tableau.component';
import {DetailUserComponent} from './component/detail-user/detail-user.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'tab',
    loadChildren: () =>
      import('./component/tableau/tableau.module').then(
        (mod) => mod.TableauModule
      ),
  },
  {
    path: 'user',
    loadChildren: () =>
      import('./component/user/user.module').then(
        (mod) => mod.UserModule
      ),
  },
  {
    path: 'detailUser/:id',
    component: DetailUserComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
