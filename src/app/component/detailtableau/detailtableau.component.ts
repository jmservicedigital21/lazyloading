import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detailtableau',
  templateUrl: './detailtableau.component.html',
  styleUrls: ['./detailtableau.component.scss']
})
export class DetailtableauComponent implements OnInit {

  users = [
    {
      id:1,
      nom: 'dupond',
      prenom: 'pierre',
      adresse: '1rue',
      tel: '123456',
      email: 'titi@toto.fr'
    },

  ]

  constructor() { }

  ngOnInit(): void {
  }

}
