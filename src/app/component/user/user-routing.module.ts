import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserComponent } from './user.component';
import {DetailUserComponent} from '../detail-user/detail-user.component';
const routes: Routes = [
  {
    path: '',
    component: UserComponent,
  },
  {
    path: 'detail',
    component: DetailUserComponent,
  },
  {
    path: 'detail/:id',
    component: DetailUserComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule {}
