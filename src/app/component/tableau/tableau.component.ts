import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tableau',
  templateUrl: './tableau.component.html',
  styleUrls: ['./tableau.component.scss']
})
export class TableauComponent implements OnInit {
  headers = ['col1', 'col2', 'col3', 'col4', 'col5',];

  rows = [
    {
      col1 : 'A',
      col2: 'b',
      col3: 'c',
      col4 : 'd',
      col5: 'e',
    },
    {
      col1 : 'f',
      col2: 'g',
      col3: 'h',
      col4 : 'i',
      col5: 'j',
    },
    {
      col1 : 'k',
      col2: 'l',
      col3: 'm',
      col4 : 'n',
      col5: 'o',
    },
    {
      col1 : 'p',
      col2: 'q',
      col3: 'r',
      col4 : 's',
      col5: 't',
    },
    {
      col1 : 'u',
      col2: 'v',
      col3: 'w',
      col4 : 'x',
      col5: 'y',
    },
    {
      col1: 'z'
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
