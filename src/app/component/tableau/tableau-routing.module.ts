import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TableauComponent } from './tableau.component';
import {DetailtableauComponent} from '../detailtableau/detailtableau.component';
const routes: Routes = [
  {
    path: '',
    component: TableauComponent,
  },
  {
    path: 'detail',
    component: DetailtableauComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TableauRoutingModule {}
