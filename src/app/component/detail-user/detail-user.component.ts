import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UsersService} from '../../services/users.service';
import {Users} from '../../model/users';

@Component({
  selector: 'app-detail-user',
  templateUrl: './detail-user.component.html',
  styleUrls: ['./detail-user.component.scss']
})
export class DetailUserComponent implements OnInit {
user: Users = <Users> {}
  constructor(private route: ActivatedRoute, private usersService: UsersService) { }

  ngOnInit(): void {
const id: number = +this.route.snapshot.paramMap.get('id');
this.usersService.getUsers().subscribe((users: Users[]) =>{
  this.user = users.find(user => user.ID = id);
  console.log('id', this.user);
});
  }

}
